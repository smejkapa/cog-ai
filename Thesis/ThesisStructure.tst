To add:

# Introduction and background
    Compare game strategy AI to classical games
        Even if classical games like go are being solved a new challenge is presented ahead of us.
        Go has really clean rules for AI to understand and each placed stone progresses the game towards end.
        This is not true in PC starategy games which has much larger branching factors and most of the actions have no
        clean short term menaing. This fact does not diminish the successes achieved in the field of Go in the slightest.
        It just presents new greater problems for us to solve - as Google is doing with StarCraft.
    What is a 4X strategy game - two player game etc.
        In this thesis we exlore the field of AI in computer strategy games specifically 4X games.
    Motivation to do AI for 4X games
        AI sometimes needs to behave like a human not only play optimally - diplomacy inc civilization.
        It is not solved problem (SC is not solved)
        AI in these games is very simple and predictable
        It does not only need to play well (as in RTS) but also behave human-like
    Which challenges do 4X games (or cog?) present
        There are many levels of the game - diplomacy, building, expansion, military, micromanagement
        We do not have a lot of time. There can be even 8 opponents and we need to finish in seconds (tens of seconds) 16 / 8 = 2 sec per player 
        The problem is very board and we can not possible solve all it's parts.
        It is better to decompose it and solve it one part at a time.
    As our testbed we choose a game called Children of Galaxy
        It is a game in development by a single developer - it is still in active development
        In GoG we control a single civilization and we try to conquer the galaxy either by force, by conquest or by culture.
        It needs AI and the AI project is open source
        The game is similar to other classical 4X games (master of orion, galactic civ, civ) - concepts from here are easily applicable elsewhere.
        We will even discuss that techniques presented here may be used in the field of RTS games. // Mozna tady nezminovat
    The rest of the thesis is organized as follows
        In the next chapter we present decomposition of the problem of AI in 4X games.
        Then we talk about work related to our problem.
        Afterwards we present our approach to solving this problem.
        Next, an implementation of this approach in the context of CoG is shown.
        We conclude by showing evaluation of different AI techniques we implemented in CoG.

# Thesis focus analysis // zduraznit ze tohle je u moje prace - vypadnout by melo, ze je jsem si zvolil co delat v te diplomce
    In this chapter we first describe what AI system is currently in CoG.
        Then we decompose the game from the AI perspective and present a subproblem which is the focus of this thesis.
    // Tady budu jeste rozepisovat co je to AI a popisovat herni mechaniky vic do hloubky nez v uvodu
    What is done now - behavior trees by Filip.
        The whole AI is programmed using behavior trees.
            This means that the AI is easy to design and behaves predictably.
            That is, however, the main problem - the AI is not adaptive enough and therefore easily exploited by the players.
            The AI is not completly deteministic. There are some ranom elements - some random choices the AI makes.
            However, this is not the ideal approach since it should choose the best option to achieve its current goal in given situation.
            For this search and planning techniques are commonly used.
        Another limitation lies in the implementation of the behavior trees.
            Behavior trees as implemented in the game do not have a Running state which is one of the biggest advantages of using behavior trees.
            This is a design choice of the author which makes the implementation simpler since the state of each tree is not saved.
            This means that each turn the AI wants to think all the trees are executed from the root. 
            1. If we want to persist some state information between turns we need to save it explicitly ourselves.
            2. We need design the behavior trees more like if-then rules and it is much more difficult to consistently return to the node we were executing in the last turn
    Decomposition of the game from AI perspective
        To decompose the problem we studied how other strategy games solve their AI.
        RTS bots such as UAlberta bot decompose the game to at first glance independent parts and try to solve these separately using so called managers.
            This is problematic because in such a complex game no two parts are really independent and the communication between managers soon proves insuficcient.
        Much better example of AI solution seems to have the Total War series.
            It too uses managers and these have different goals. But since the resources to attain these goals are shared the managers present their tasks and vote.
            If more managers want to do the same task resources are allocated to it and it is then executed. For the resource allocation MCTS is used.
            One of the key parts of most strategy games is research. 
            Research goals should align with needs of the managers and with the nature of given civilization.
            There are usually several ways to progress with the research... // Mozna to tu moc nerozebirat, ale reseni bychom meli, takze to rozebrat muzeme
        How did we select our subproblem from all this.
            If we wanted to do the task system, a micro estimator would be necessary. 
            The rating of tasks would be greatly dependent on the quality of this micro estimator - it is necessary to know the risk of sending a fleet somewhere.
            A natural first step would be to solve this problem bottom up and implement a micro estimator.
            We also wanted to select a problem that is modular enough that ongoing game development would not break our architecture and solutions constantly.
        We set the goals of this thesis as follows:
            Analysis of possible approaches to micromanagement
            Improvement of one of these approaches
            Implementation of a micro estimator (evaluator) game component
            Example integration of this component into the game
            Implementation of evaluation framework for different micro approaches
            Evaluation of different micro approaches in this framework

# Related work
// Uvod - RTSka jsou podobne 4X hram (v cem a jak?)
// Related jsou searche
// Rozdelit na tools (algoritmy) a realne aplikace
// Listnout vysledky ktere jsou - tady mame prace - rozebereme si je pozdeji

# General approach    // Abstraktni povidani o AI v 4X hrach (CoG)
    // V uvodu - tady si ukazeme reseni problemu ktere jsme si urcili
    Simple approaches - scripts
        Scripts are small (or large) predefined designed behaviors.
        Many commercial video games use scripts in some form in AI.
        FSMs - each state is a script and we switch between them.
        Behavior trees - compose one big behavior from many small ones (scripts). // Tady se i trosku vysvetli stromy
        GOAP - plan in action (script) space. // Taky provadi search
        // Zaver - vzdy se rozhodujeme ve skriptech
        // Tady mit mozna motivaci - co kdyz nechceme designovat kdy se behavior zmeni, ale radi bychom to nechali na programu?
    From minimax to MCTS    // Mozna by stacilo popsat jen MCTS?
        Advantages of search
            Search is especially advantageous for us since the game may change drastically during the development and we want our AI to remain viable
            Search can adapt to current situation and to what player does
        Disadvantages of search
            It is very hard to control specific elements of behavior - the AI may optimize but it may not look very natural; e.g. back and forth with units etc.
    Different forms of search
        Search in action space - branching factor comparison of Go vs CoG
        Search in script space - branching factor comparison or action space vs script space
            Search in script space means that the behavior switching is controlled by the search and not by designers.
            Search just tries different moments in time when to switch the behavior and tries to find the optimal moment (and optimal behavior).
        Search in script space and clustering    // To nepouzivam, to sem asi nepatri, ale je to dalsi typ co by sel pouzit
        Portfolio greedy search // Dava smysl mit vic skrpitu - zminit skripty ktere pouziva
        Alpha beta in script space // A co iterative deepening?
            This would not work because AB needs full playouts (or heuristic) and if we stop in the middle of search because time run our the result will be bad. 
            We are not sure when to stop the playout and use the heuristic. (We tried to use heuristics in MCTS and it did not work well).
        MCTS in script space - this works because it uses random playouts and the search is adaptive - we can stop whenever we want.
    // Zaver je ze budeme pouzivat MCTS na script space - dale se poivae jak to budeme delat

# MCTS in CoG
    What does MCTS mean in the context of CoG
        Random playout in script space
        What would be random playout in action space - it would not make sense - units can just move
        Which scripts do we use // Mozna se bude kryt s povidanim vyse
    Improvement
        Our improvements of MCTS in script space
        Compare to Churchill Portfolio greedy search
        What is the limiting factor in our case?
        // Nekde zminit i naive search
        It is not so much the branching factor but the fact that playouts are expensive.
        What about alpha beta in the Script space? // Mozna staci nahore

# Implementation    // High level architetura AIcka a toolu, ktere jsme vytvorili 
    Current AI
        Current AI is opensource on GitHub, but you still need the game to be able to compile it.
        The project is a single C# library.
        Functionality of the nodes is 
    (Zkracene) Behavior tree visualization    // Sem spis takovy vytah architektury, how to by melo asi byt az v uzivatelske dokumentaci
        Tools are important
        Behavior trees are encoded as XML. That is nice because XML implicitly has a tree structure.
        It is not very useful, however, if we want to see what are the trees doing and it is hard to resolve references (we need to open other files).
        To overcome this limitation we developed a behavior tree visualizer and serializer.
        These are python scripts which automatically transform the xml files to a gml format.
        This format can be then read and visualized by a free graph editor yEd.
        We chose yEd because it is easy to use, free, and handles tree graph layout on its own - we do not have to position the nodes ourselves.
        The visualizer script also optionally follows references so we can have one big behavior tree to easily comprehend what is happening.
        We also want to edit the behavior trees which is equally difficult in plain XML.
        For this we use yEd again. yEd is customizable so we can add our own nodes to the palette and use yEd as a behavior tree editor.
        We can create a node, fill-in name and parameters, connect it to its parent and position it to its siblings just as you would expect in a behavior tree editor.
        When the tree is done you must save it as .gml file and run the writer script which will create a xml file from the gml. This xml can then be used in game.
    Children of the Galaxy Micro Simulator (CMS)
    // Velka limitace her je ze jsou potreba vizualizace a modely k temto hram nez se da zacit aplikovat teoreticke poznatky
        General // Vzdy kdyz jsem neco udelal tak hned za to napsat jak mi to pomohlo - na co je to dobre
            Created as a library which can be added to the AI as a dependency.
            First we wanted to write the CMS in C++ for the fastest possible search.
            But the AI should be easy to modify by the community and the game is multi-platform to not over-complicate things and to allow the widest userbase possible
                we decided to write it in C# as the reset of the game. 
            The library is state-less and it has very simple and clean API
            CMS provides us with variety of AI players which can simulate the game and recommend the next best action.
            We just need to create one such a player and give it a game state. Then we can ask it to return the best action and we can perform this action in game.
            CMS has do dependencies on the game. If the game wants to use CMS the game depends on it, not the other way around.
            This allows us to create a CLI, connect the CMS and simulate games without the need of the game.
            That of course means we had to implement the core game mechanics again (since we do not have the game).
            That would be however necessary anyways even if we had dependencies on the game.
            We use search - it needs small game state and actions need to do only what's necessary - actions in game work with animations etc.
            We do not have access to the source code of the game - we implemented the actions (and slim game state) from decompiled game dlls. This was not trivial
        Architecture
            UML of it all
            GameState
            Unit
            Action
            Players vs AI
            Pathfinding
            All players
                Scripts
                PGS - caching was introduced which greatly improved the performance
                MCTS + hull value improvement
                
# Experiments
    // Cim vic tim lip
    // Udelat hodne konfiguraku a rozjet experimenty
    // Aicka: Skpripy, PG

    // Udelat video z training mission
    // Nevadi ze to nebude ve hre - mela by tam byt jedna konkretni traning mise ve ktere se ukaze AI na urcitem buildu
    // Mit scenar MCTS vs NOKAV

# Conclusion and future work
    // Napojit na cile - vsechny byly splneny a kde
    // Jde psat i v textu - ted jsme splnili cil XY

        

