library("ggplot2")
library("reshape2")
d <- data.frame(playoutValue=c(-158, 0, 86), nodeValue=c(-1, 0, 1))

px <- pretty(d$playoutValue)
py <- pretty(d$nodeValue)

p <- ggplot(d, aes(x=playoutValue, y=nodeValue, color=1)) +
  geom_line(size = 2) +
  geom_point(size = 4) +
  scale_x_continuous(breaks=px, limits=range(px)) +
  scale_y_continuous(breaks=py, limits=range(py)) +
  ggtitle("Example mapping") +
  theme(
    legend.position="none", plot.title = element_text(hjust = 0.5),
    text = element_text(size = 30, colour="black")
  )

ggsave("d:/tmp/cog/results/mapping.png", width=7, height=7)
