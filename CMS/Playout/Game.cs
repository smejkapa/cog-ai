﻿using System.CodeDom;
using System.Collections.Generic;
using System.Diagnostics;
using CMS.Pathfinding;
using CMS.Players;

namespace CMS.Playout
{
    /// <summary>
    /// Static class which can simulate a combat scenario
    /// </summary>
    public static class Game
    {
        /// <summary>
        /// Simulates a combat scenario from given parameters.
        /// Ends either when one player does not have any units or when <paramref name="roundLimit"/> is reached.
        /// </summary>
        /// <param name="environment">Environment and game state of the combat scenario.
        /// Is changed during the simulation.</param>
        /// <param name="players">Players involved in this combat.</param>
        /// <param name="roundLimit">Maximum number of rounds for the simulation.</param>
#if BENCHMARK
        /// <param name="detailedStats">Whether detailed stats should be generated.</param>
#endif
        /// <returns>Result of the simulation.</returns>
        public static GameResult Playout(GameEnvironment environment, List<Player> players, int roundLimit = int.MaxValue
#if BENCHMARK
            , bool detailedStats = false
#endif 
            )
        {
#if BENCHMARK
            var roundTimes = new List<double>[players.Count];
            for (int i = 0; i < roundTimes.Length; i++)
            {
                roundTimes[i] = new List<double>();
            }
            var sw = new Stopwatch();
#endif
            var round = 0;
            while (round < roundLimit &&
                environment.GameState.ActivePlayerUnits.Count > 0 &&
                environment.GameState.OtherPlayerUnits.Count > 0)
            {
#if BENCHMARK
                int lastActive = environment.GameState.ActivePlayer;
                sw.Restart();
#endif
                ++round;
                players[environment.GameState.ActivePlayer].MakeActions(environment);
                environment.GameState.NextTurn();
#if BENCHMARK
                sw.Stop();
                if (detailedStats)
                    roundTimes[lastActive].Add(sw.Elapsed.TotalMilliseconds);
#endif
            }

            var result = environment.GameState.GetResult();

            return new GameResult(round, result, result == GameState.DRAW
#if BENCHMARK
                , roundTimes
#endif
                );
        }
    }
}
