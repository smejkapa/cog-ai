﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using CMS.Actions;
using CMS.Micro.Scripts;
using CMS.Units;

namespace CMS.ActionGenerators
{
    public class ScriptActionGenerator : IActionGenerator
    {
        private readonly List<IScript> _scripts;

        public ScriptActionGenerator(List<IScript> scripts)
        {
            _scripts = scripts;
        }

        public List<ActionStatePair> GenerateActions(GameEnvironment environment)
        {
            var unitCount = environment.GameState.ActivePlayerUnits.Count;
            return GenAllActions(0, unitCount, new List<ActionCms>(), environment).ToList();
        }

        public IEnumerable<ActionStatePair> EnumerateActions(GameEnvironment environment)
        {
            var unitCount = environment.GameState.ActivePlayerUnits.Count;
            foreach (ActionStatePair playerAction in GenAllActions(0, unitCount, new List<ActionCms>(), environment))
            {
                yield return playerAction;
            }
        }

        private IEnumerable<ActionStatePair> GenAllActions(int unitIdx, int unitCount, IList<ActionCms> current, GameEnvironment environment)
        {
            if (unitIdx == unitCount || 
                environment.GameState.GetResult() != GameState.WINNER_NONE)
            {
                yield return new ActionStatePair {PlayerAction = new List<ActionCms>(current), Environment = environment};
            }
            else
            {
                foreach (IScript script in _scripts)
                {
                    GameEnvironment envClone = environment.DeepCloneState();
                    IEnumerable<Unit> enemyUnits = envClone.GameState.OtherPlayerUnits.Values;
                    List<Unit> units = envClone.GameState.ActivePlayerUnits.Values.ToList();

                    ActionCms action = script.MakeAction(envClone, enemyUnits, units[unitIdx]);
                    units[unitIdx].Script = script;
                    current.Add(action);

                    foreach (ActionStatePair ret in GenAllActions(unitIdx + 1, unitCount, current, envClone))
                    {
                        yield return ret;
                    }
                    current.RemoveAt(current.Count - 1);
                }
            }
        }
    }
}
