﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.IO;
using CMS.ActionGenerators;
using CMS.Actions;
using CMS.GameStateEval;
using CMS.Players;
using CMS.Playout;
using System.Linq;
using System.Text;
using System.Xml;
using QuickGraph;
using QuickGraph.Serialization;

namespace CMS.Micro.Search.MCTS
{
    public class Mcts
    {
        private readonly IActionGenerator _actionGenerator;
        private readonly int _timeLimit;
        private static readonly double C = 1/Math.Sqrt(2);
        private readonly List<Player> _players;
        private readonly int _playoutRoundLimit;
        private readonly IGameStateEvaluator _evaluator;
        private readonly bool _useHullAsValue;

        public int TotalIterCount { get; private set; }

        private static int _run;

        public Mcts(
            IActionGenerator actionGenerator, 
            int timeLimit, 
            List<Player> players, 
            bool useHullAsValue,
            int playoutRoundLimit = int.MaxValue)
        {
            _actionGenerator = actionGenerator;
            _timeLimit = timeLimit;
            _players = players;
            _playoutRoundLimit = playoutRoundLimit;
            _evaluator = new SimpleGameStateEvaluator();
            _useHullAsValue = useHullAsValue;
        }

        internal ICollection<ActionCms> GetActions(GameEnvironment environment)
        {
            Stopwatch sw = Stopwatch.StartNew();
            TotalIterCount = 0;
            var rootActionState = new ActionStatePair {Environment = environment};
            IEnumerable<ActionStatePair> rootChildActions = _actionGenerator.EnumerateActions(rootActionState.Environment);

            var root = new MctsNode(null, rootActionState, rootChildActions);

            while (sw.ElapsedMilliseconds < _timeLimit)
            {
                ++TotalIterCount;
                // Selection and Expansion
                MctsNode selectedNode = SelectAndExpand(root);

                // Playout
                GameEnvironment selectedStateClone = selectedNode.CurrentActionState.Environment.DeepCloneState();
                GameResult result = Game.Playout(selectedStateClone, _players, _playoutRoundLimit);
                double value = EvaluateGame(result, selectedStateClone);

                // Backpropagation
                BackpropagateResults(selectedNode, value);
            }

            MctsNode bestNode = SelectBestChild(root, environment.GameState.ActivePlayer, (n, p) => p == 0 ? n.VisitedCount : -n.VisitedCount);
            //Console.WriteLine(bestNode.Value);
            //Console.WriteLine(bestNode.VisitedCount);
            //Console.WriteLine(bestNode.Value / bestNode.VisitedCount);
            var actions = bestNode.CurrentActionState.PlayerAction;

            //Console.WriteLine(TotalIterCount);
            //Console.WriteLine("---");
            //PlotTreeGraphGv(root);

            return actions;
        }

        private MctsNode SelectAndExpand(MctsNode node)
        {
            Debug.Assert(node != null);
            while (true)
            {
                // If node is not fully expanded - generate new chind node with this new action and return it
                MctsNode newChild;
                if (node.TryGetNextChild(_actionGenerator, out newChild))
                {
                    return newChild;
                }
                else if (node.IsTerminal)
                {
                    return node;
                }

                // Else find child node with highest value and call Selection recursively
                int player = node.CurrentActionState.Environment.GameState.ActivePlayer;
                MctsNode bestChild = SelectBestChild(node, player, Ucb);

                if (bestChild == null)
                    return null;

                node = bestChild;
            }
        }

        private MctsNode SelectBestChild(MctsNode node, int player, Func<MctsNode, int, double> evalFunc)
        {
            double bestScore = 0.0;
            MctsNode bestChild = null;
            foreach (MctsNode child in node.Children)
            {
                //double score = Ucb(child);
                double score = evalFunc(child, player);
                if (bestChild == null ||
                    (score > bestScore && player == 0) ||
                    (score < bestScore && player == 1))
                {
                    bestScore = score;
                    bestChild = child;
                }
            }

            return bestChild;
        }

        private void BackpropagateResults(MctsNode node, double value)
        {
            do
            {
                node.VisitedCount++;
                if (_useHullAsValue)
                {
                    if (value < 0)
                        node.Value += value / node.UnitHulls[1];
                    else
                        node.Value += value / node.UnitHulls[0];
                }
                else
                {
                    node.Value += value;
                }
                node = node.Parent;
            } while (node != null);
        }

        internal static double Ucb(MctsNode node, int player)
        {
            double exploit = node.Value / node.VisitedCount;
            double explore = C * Math.Sqrt(Math.Log(node.Parent.VisitedCount) / node.VisitedCount);
            return player == 0 ? (exploit + explore) : (exploit - explore);
        }

        private double EvaluateGame(GameResult result, GameEnvironment environment)
        {
            if (_useHullAsValue)
            {
                var hull = environment.GameState.Units[0].Values.Sum(x => x.Hull)
                           - environment.GameState.Units[1].Values.Sum(x => x.Hull);
                return hull;
            }
            else
            {
                if (result.Winner == GameState.WINNER_NONE)
                {
                    var score = _evaluator.EvaluateGameState(environment.GameState, 0);
                    if (score > 0.0)
                        return 1.0;
                }
                else
                {
                    if (result.IsDraw)
                        return 0.0;
                    else if (result.Winner == 0)
                        return 1.0;
                }

                // We lost or are in bad position
                return -1.0;
            }
        }

        private void PlotTreeGraph(MctsNode root)
        {
            var g = new AdjacencyGraph<MctsNode, Edge<MctsNode>>();

            AddToGraph(g, root);

            using (var xwriter = XmlWriter.Create($"d:/tmp/cog/graphTest_{_run}.graphml"))
            {
                g.SerializeToGraphML<MctsNode, Edge<MctsNode>, AdjacencyGraph<MctsNode, Edge<MctsNode>>>(xwriter);
            }
            ++_run;
        }

        private void AddToGraph(AdjacencyGraph<MctsNode, Edge<MctsNode>> graph, MctsNode node)
        {
            foreach (MctsNode child in node.Children)
            {
                if (child.Parent != null)
                    graph.AddVerticesAndEdge(new Edge<MctsNode>(child.Parent, child));

                AddToGraph(graph, child);
            }
        }

        private void PlotTreeGraphGv(MctsNode root)
        {
            var graph = new StringBuilder();
            graph.AppendLine("digraph g {");

            AddToGraphGv(root, graph);

            graph.AppendLine("}");

            using (var sw = new StreamWriter($"d:/tmp/cog/graphTest_{_run:D3}.gv"))
            {
                sw.Write(graph.ToString());
            }
            ++_run;
        }

        private void AddToGraphGv(MctsNode node, StringBuilder graph)
        {
            graph.AppendLine($"{node.Id} [shape=square,label=\"{node.LabelData}\"]");
            foreach (MctsNode child in node.Children)
            {
                if (child.Parent != null)
                {
                    graph.AppendLine($"{node.Id} -> {child.Id}");
                }
                AddToGraphGv(child, graph);
            }
        }

        public override string ToString()
        {
            return $"MCTS hull: {_useHullAsValue}, time: {_timeLimit}";
        }
    }
}
