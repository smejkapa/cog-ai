﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Xml.Serialization;
using CMS.ActionGenerators;
using CMS.Actions;

namespace CMS.Micro.Search.MCTS
{
    internal class MctsNode
    {
        private static long _id;
        public long Id { get; }

        public float[] UnitHulls { get; }

        public MctsNode(MctsNode parent, ActionStatePair thisActionState, IEnumerable<ActionStatePair> childActions)
        {
            Parent = parent;
            CurrentActionState = thisActionState;
            ChildActions = childActions;
            Children = new List<MctsNode>();
            _currentActionEnum = ChildActions.GetEnumerator();
            Id = _id++;
            UnitHulls = new[]
            {
                thisActionState.Environment.GameState.Units[0].Values.Sum(x => x.Hull),
                thisActionState.Environment.GameState.Units[1].Values.Sum(x => x.Hull)
            };
        }

        public MctsNode Parent { get; }
        public List<MctsNode> Children { get; }

        public ActionStatePair CurrentActionState { get; }

        public int VisitedCount { get; set; }

        public double Value { get; set; }

        [XmlAttribute("LabelData")]
        public string LabelData
        {
            get
            {
                string scriptAlloc = "---";
                if (CurrentActionState.Environment.GameState.OtherPlayerUnits.Count > 0)
                {
                    scriptAlloc = CurrentActionState.Environment.GameState.OtherPlayerUnits.Values
                        .Select(x => x.Script != null ? x.Script.ToShortName() : "X")
                        .Aggregate((a, b) => a + b);
                }

                double ucbValue = Parent == null ? 0 : Mcts.Ucb(this, CurrentActionState.Environment.GameState.OtherPlayer);

                var sb = new StringBuilder();
                //<html> <strong class="tag">Behavior</strong><br /><span class="attrib">Name = BrainAttack</span></html>
                //sb.Append("<html>");

                sb.Append($@"Visit: {VisitedCount}\n");
                sb.Append($@"{scriptAlloc}\n");
                sb.Append($@"Value: {Value}\n");
                sb.Append($@"Ucb: {ucbValue}\n");
                sb.Append($@"Terminal: {IsTerminal}\n");

                //sb.Append("</html>");
                return sb.ToString();
            }
        }

        public IEnumerable<ActionStatePair> ChildActions { get; }
        private readonly IEnumerator<ActionStatePair> _currentActionEnum;

        public bool IsTerminal 
            => CurrentActionState.Environment.GameState.GetResult() != GameState.WINNER_NONE;

        public bool TryGetNextChild(IActionGenerator generator, out MctsNode child)
        {
            if (!_currentActionEnum.MoveNext())
            {
                child = null;
                return false;
            }
            else
            {
                ActionStatePair playerAction = _currentActionEnum.Current;

                child = new MctsNode(this, playerAction, generator.EnumerateActions(playerAction.Environment));
                playerAction.Environment.GameState.NextTurn();
                Children.Add(child);
                return true;
            }
        }
    }
}
