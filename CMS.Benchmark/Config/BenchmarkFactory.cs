﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.IO;
using System.Linq;
using System.Xml;
using System.Xml.Linq;
using System.Xml.Schema;
using CMS.ActionGenerators;
using CMS.Benchmark.Exceptions;
using CMS.Micro.Scripts;
using CMS.Micro.Search;
using CMS.Micro.Search.MCTS;
using CMS.Players;
using CMS.Players.Script;
using CMS.Players.Search;

namespace CMS.Benchmark.Config
{
    /// <summary>
    /// Factory class which fills and creates a benchmark.
    /// </summary>
    internal static class BenchmarkFactory
    {
        private static readonly string _benchmarkDbDir = "Resources/Benchmarks";
        private static readonly string _benchmarkSetDir = "Resources/BenchmarkSets";
        private static readonly string _battleSetDbDir = "Resources/BattleSets";
        private static readonly string _aiDbDir = "Resources/AIs";

        private static readonly string _benchmarkSchemaFile = Path.Combine(_benchmarkDbDir, "Benchmark.xsd");
        private static readonly string _benchmarkSetSchemaFile = Path.Combine(_benchmarkSetDir, "BenchmarkSet.xsd");
        private static readonly string _battleSetSchemaFile = Path.Combine(_battleSetDbDir, "BattleSet.xsd");
        private static readonly string _aiDbSchemaFile = Path.Combine(_aiDbDir, "AI.xsd");

        private static readonly XmlSchemaSet _benchmarkSchema = new XmlSchemaSet();
        private static readonly XmlSchemaSet _benchmarkSetSchema = new XmlSchemaSet();
        private static readonly XmlSchemaSet _battleSetSchema = new XmlSchemaSet();
        private static readonly XmlSchemaSet _aiSchemaSet = new XmlSchemaSet();

        private static bool _isInitialized;

        private static void Initialize()
        {
            if (!_isInitialized)
            {
                _isInitialized = true;
                _benchmarkSchema.Add("", XmlReader.Create(new StreamReader(_benchmarkSchemaFile)));

                _benchmarkSchema.Add(XmlValidation.TypeSchema);
                _benchmarkSetSchema.Add("", XmlReader.Create(new StreamReader(_benchmarkSetSchemaFile)));

                _battleSetSchema.Add("", XmlReader.Create(new StreamReader(_battleSetSchemaFile)));

                _aiSchemaSet.Add("", XmlReader.Create(new StreamReader(_aiDbSchemaFile)));
                _aiSchemaSet.Add(XmlValidation.TypeSchema);
            }
        }

        /// <summary>
        /// Creates and returns fully initialized benchmark specified in file with name <paramref name="id"/>.
        /// </summary>
        /// <param name="id">Name of the file with the battle config.</param>
        public static List<Benchmark> MakeBenchmarkSet(string id)
        {
            Initialize();
            string docFile = Path.Combine(_benchmarkSetDir, id + ".xml");
            ResourceValidation.CheckResource(docFile);
            XDocument doc = XDocument.Load(docFile, LoadOptions.SetLineInfo);
            XmlValidation.ValidateDocument(doc, _benchmarkSetSchema, docFile);

            IEnumerable<XElement> benchmarksXml = doc.Root.Elements("Benchmark");
            var benchmarks = new List<Benchmark>();
            foreach (XElement benchmarkXml in benchmarksXml)
            {
                try
                {
                    benchmarks.Add(MakeBenchmark(benchmarkXml.Attributes("Id").First().Value));
                }
                catch (ResourceMissingException rme)
                {
                    throw new InvalidResourceReferenceException(rme.Resource, docFile);
                }
            }

            return benchmarks;
        }

        private static Benchmark MakeBenchmark(string id)
        {
            string docFile = Path.Combine(_benchmarkDbDir, id + ".xml");
            ResourceValidation.CheckResource(docFile);
            XDocument doc = XDocument.Load(docFile, LoadOptions.SetLineInfo);
            XmlValidation.ValidateDocument(doc, _benchmarkSchema, docFile);

            // Repeats
            int repeats = int.Parse(doc.Root.Elements("Repeats").First().Value);

            // Handle battles
            var battleSetsXml = doc.Root.Elements("BattleSet");
            var battles = new List<BattleSettings>();
            foreach (XElement battleSet in battleSetsXml)
            {
                try
                {
                    battles.AddRange(MakeBattles(battleSet.Attributes("Id").First().Value, repeats));
                }
                catch (ResourceMissingException rme)
                {
                    throw new InvalidResourceReferenceException(rme.Resource, docFile);
                }
            }

            // Handle players
            var playersXml = doc.Root.Elements("Player");
            var players = new List<Player>(2) {null, null};
            var playerNames = new List<string>(2) {"NO_NAME", "NO_NAME"};
            foreach (XElement playerXml in playersXml)
            {
                string playerName;
                Player player = MakePlayer(playerXml, out playerName);
                var playerIdx = int.Parse(playerXml.Attributes("Index").Single().Value);
                players[playerIdx] = player;
                playerNames[playerIdx] = playerName;
            }

            // Handle max rounds
            int maxRounds = int.Parse(doc.Root.Elements("MaxRounds").First().Value);

            // Handle is symmetric
            bool isSymmetric = bool.Parse(doc.Root.Elements("IsSymmetric").First().Value);

            var benchmark = new Benchmark(battles, players, maxRounds, isSymmetric, id, playerNames);
            return benchmark;
        }

        private static List<BattleSettings> MakeBattles(string id, int repeats)
        {
            string docFile = Path.Combine(_battleSetDbDir, id + ".xml");
            ResourceValidation.CheckResource(docFile);
            XDocument doc = XDocument.Load(docFile, LoadOptions.SetLineInfo);
            XmlValidation.ValidateDocument(doc, _battleSetSchema, docFile);

            IEnumerable<XElement> battlesXml = doc.Root.Elements("Battle");
            var battles = new List<BattleSettings>();
            foreach (XElement battle in battlesXml)
            {
                try
                {
                    battles.Add(BattleFactory.MakeBattle(battle.Attributes("Id").First().Value, repeats));
                }
                catch (ResourceMissingException rme)
                {
                    throw new InvalidResourceReferenceException(rme.Resource, docFile);
                }
            }

            return battles;
        }

        private static Player MakePlayer(XElement playerXml, out string playerName)
        {
            XElement ai;
            XElement aiElement = playerXml.Elements("AI").SingleOrDefault();
            if (aiElement != null)
            {
                ai = aiElement.Elements().Single();
                playerName = ai.Name.LocalName;
            }
            else // We have reference to AI
            {
                ai = ResolveAIRef(playerXml.Elements("AIRef").Single());
                playerName = playerXml.Elements("AIRef").Single().Attributes("Id").Single().Value;
            }
            string name = ai.Name.LocalName;
            switch (name)
            {
                case "NOKAV":
                    return new ScriptedPlayer<NOKAV>();
                case "Kiter":
                    return new ScriptedPlayer<Kiter>();
                case "KiterSimple":
                    //return new ScriptedPlayer<KiterSimple>();
                    throw new InvalidEnumArgumentException($"{name} is not supported yet.");
                case "NOKAVSimple":
                    //return new ScriptedPlayer<NOKAVSimple>();
                    throw new InvalidEnumArgumentException($"{name} is not supported yet.");
                case "PortfolioGreedySearch":
                    return MakePGSPlayer(ai);
                case "ScriptMCTS":
                    return MakeScriptMctsPlayer(ai);
                case "RandomScriptSelector":
                    return MakeRandomScriptSelectorPlayer(ai);
                default:
                    throw new ArgumentOutOfRangeException(nameof(playerXml));
            }
        }

        private static Player MakeRandomScriptSelectorPlayer(XElement playerXml)
        {
            IEnumerable<XElement> portfolioXml = playerXml.Elements("Script");
            var portfolio = new List<IScript>();
            foreach (XElement script in portfolioXml)
            {
                portfolio.Add(MakeScript(script.Value));
            }

            return new RandomScriptSelectPlayer(portfolio);
        }

        private static XElement ResolveAIRef(XElement aiRefXml)
        {
            string aiId = aiRefXml.Attributes("Id").Single().Value;
            return MakeAi(aiId);
        }

        private static XElement MakeAi(string id)
        {
            string docFile = Path.Combine(_aiDbDir, id + ".xml");
            ResourceValidation.CheckResource(docFile);
            XDocument doc = XDocument.Load(docFile);
            XmlValidation.ValidateDocument(doc, _aiSchemaSet, docFile);

            XElement ai = doc.Root.Elements().Single();
            return ai;
        }

        private static Player MakePGSPlayer(XElement playerXml)
        {
            int imprCount = int.Parse(playerXml.Attributes("ImproveCount").First().Value);
            int responseCount = int.Parse(playerXml.Attributes("ResponseCount").First().Value);
            IScript defaultScript = MakeScript(playerXml.Attributes("DefaultScript").First().Value);
            int timeLimit = int.Parse(playerXml.Attributes("TimeLimit").First().Value);
            int maxTurns = int.Parse(playerXml.Attributes("MaxTurns").First().Value);

            IEnumerable<XElement> portfolioXml = playerXml.Elements("Script");
            var portfolio = new List<IScript>();
            foreach (XElement script in portfolioXml)
            {
                portfolio.Add(MakeScript(script.Value));
            }

            var search = new PortfolioGreedySearch(portfolio, imprCount, responseCount, defaultScript, timeLimit, maxTurns);
            return new PortfolioGreedyPlayer<PortfolioGreedySearch>(search);
        }

        private static Player MakeScriptMctsPlayer(XElement playerXml)
        {
            int timeLimit = int.Parse(playerXml.Attributes("TimeLimit").First().Value);
            int maxTurns = int.Parse(playerXml.Attributes("MaxTurns").First().Value);
            bool useHullAsValue = bool.Parse(playerXml.Attributes("UseHullAsValue").First().Value);
            IEnumerable<XElement> portfolioXml = playerXml.Elements("Script");
            var portfolio = new List<IScript>();
            foreach (XElement script in portfolioXml)
            {
                portfolio.Add(MakeScript(script.Value));
            }

            var players = new List<Player>
            {
                new RandomScriptSelectPlayer(portfolio),
                new RandomScriptSelectPlayer(portfolio)
            };
            var actionGenerator = new ScriptActionGenerator(portfolio);
            var search = new Mcts(actionGenerator, timeLimit, players, useHullAsValue, maxTurns);
            return new MctsPlayer<Mcts>(search);
        }

        private static IScript MakeScript(string scriptName)
        {
            switch (scriptName)
            {
                case "NOKAV":
                    return new NOKAV();
                case "Kiter":
                    return new Kiter();
                case "KiterSimple":
                    //return new KiterSimple();
                    throw new InvalidEnumArgumentException($"{scriptName} is not supported yet.");
                case "NOKAVSimple":
                    //return new NOKAVSimple();
                    throw new InvalidEnumArgumentException($"{scriptName} is not supported yet.");
                default:
                    throw new ArgumentOutOfRangeException(nameof(scriptName));
            }
        }
    }
}
