﻿//#define DETAILED

using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Globalization;
using System.IO;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;
using CMS.Players;
using CMS.Playout;
using Npgsql;
using System.Configuration;
using CMS.Pathfinding;

namespace CMS.Benchmark
{
    /// <summary>
    /// Represents a single benchmark from "Resources/Benchmarks"
    /// </summary>
    internal class Benchmark
    {
        private readonly string CONNECTION_STRING;
        private readonly bool USE_DB;
        private readonly List<BattleSettings> _battles;
        private readonly List<Player> _players;
        private readonly List<string> _playerNames;
        private readonly int _roundMax;
        private readonly bool _isSymmetric;
        private readonly string _name;

        /// <summary>
        /// Creates a benchmark with given parameters.
        /// </summary>
        /// <param name="battles">Battles to be run in this benchmark.</param>
        /// <param name="players">Players which battle in this benchmark.</param>
        /// <param name="roundMax">Maximum number of rounds for each batte.</param>
        /// <param name="isSymmetric">Should the players be switched and play again?</param>
        /// <param name="name">File name of the benchmark.</param>
        /// <param name="playerNames">Names of the players.</param>
        public Benchmark(List<BattleSettings> battles, List<Player> players, int roundMax, bool isSymmetric, string name, List<string> playerNames)
        {
            _battles = battles;
            _players = players;
            _roundMax = roundMax;
            _isSymmetric = isSymmetric;
            _name = name;
            _playerNames = playerNames;
            USE_DB = bool.Parse(ConfigurationManager.AppSettings["useDatabase"]);
            CONNECTION_STRING = USE_DB ? ConfigurationManager.AppSettings["connectionString"] : string.Empty;
        }

        /// <summary>
        /// Starts this benchmark.
        /// </summary>
        /// <param name="resultDir">Where should the results be outputted.</param>
        public void Run(string resultDir)
        {
            using (var dbConnection = new NpgsqlConnection(CONNECTION_STRING))
            {
                if (USE_DB)
                {
                    dbConnection.Open();
                }

                Console.WriteLine("Benchmark started");
                Console.WriteLine($"{_players[0]}");
                Console.WriteLine($"{_players[1]}");

                var sumResults = new Results();

                Stopwatch totalTime = Stopwatch.StartNew();
                int battleNumber = 1;

                var resultsFile = Path.Combine(resultDir, $"{_name}.csv");
                if (File.Exists(resultsFile))
                    Console.WriteLine($"[WARNING] File {resultsFile} already exists, overwriting");

                using (var resultsWriter = new StreamWriter(resultsFile))
                {
                    resultsWriter.WriteLine(
                        "battleName; repeats; p1win; p2win; p1symWin; p2symWin; p1hull; p2hull; minRounds; maxRounds; medianRounds; avgRounds;"
                        + "p1minRoundTime; p1maxRoundTime; p1medianRoundTime; p1avgRoundTime"
                        + "p2minRoundTime; p2maxRoundTime; p2medianRoundTime; p2avgRoundTime"
                    );
                    foreach (BattleSettings battle in _battles)
                    {
                        var results = new Results();

                        Console.WriteLine($"Battle {battle.Name} started for {battle.Repeats} repeats");
                        for (int r = 0; r < battle.Repeats; r++)
                        {
                            var hulls = new[] {0.0f, 0.0f};
                            for (int iter = 0; iter < 2; iter++)
                            {
                                Console.WriteLine($"  Iter number {battleNumber++} started");
                                GameEnvironment envi = battle.GameEnvironment.DeepCloneState();
                                if (iter == 1)
                                {
                                    SwapUnitsAndActive(envi);
                                }

                                Stopwatch playoutSw = Stopwatch.StartNew();
                                GameResult result = Game.Playout(envi, _players, _roundMax, detailedStats: true);
                                playoutSw.Stop();
                                results.RoundCounts.Add(result.RoundCount);
#if DETAILED
                    Console.WriteLine($"Results:");

                    Console.WriteLine($"  Rounds: {result.RoundCount}");
#endif
                                if (!result.IsDraw)
                                {
                                    results.WinCounts[result.Winner]++;
                                    float totalHull = envi.GameState.Units[result.Winner].Values.Sum(u => u.Hull);
                                    results.HullRemaining[result.Winner] += totalHull;
                                    hulls[result.Winner] = totalHull;
                                    Console.WriteLine($"    Winner: {result.Winner} {_players[result.Winner]}");
#if DETAILED
                        Console.WriteLine($"  Units left: {envi.GameState.Units[result.Winner].Count}");
                        Console.WriteLine($"  Hull sum: {totalHull}");
#endif
                                }
                                else
                                {
                                    results.WinCounts[2]++;
#if DETAILED
                        Console.WriteLine($"  Draw: {result.IsDraw}");
#endif
                                }
#if DETAILED
                    for (int i = 0; i < _players.Count; i++)
                    {
                        Console.WriteLine("  ---------------------------------");
                        Console.WriteLine($"  Rounds for player: {_players[i]}");
                        Console.WriteLine(
                            $"    min/max avg/median round time: {result.RoundTimes[i].Min():0.000}/{result.RoundTimes[i].Max():0.000} " +
                            $"{result.RoundTimes[i].Average():0.000}/{result.RoundTimes[i].Median():0.000}");
                        Console.WriteLine("  " + string.Join(" ", result.RoundTimes[i]));
                    }
                    Console.WriteLine("  ---------------------------------");
                    Console.WriteLine($"Cahce hit rate: {Pathfinder.CacheHitCount / (double)Pathfinder.TotalQueries * 100.0:0.00}% {Pathfinder.CacheHitCount} / {Pathfinder.TotalQueries}");              
                    Console.WriteLine($"Finished in: {playoutSw.Elapsed.ToString()}");
#endif
                                for (int i = 0; i < _players.Count; ++i)
                                {
                                    results.RoundTimes[i].AddRange(result.RoundTimes[i]);
                                }

                                if (!_isSymmetric)
                                {
                                    break;
                                }
                                else if (iter == 1)
                                {
                                    var symWinner = hulls[0] > hulls[1] ? 0 : hulls[0] < hulls[1] ? 1 : 2;
                                    results.SymWinCounts[symWinner]++;
                                }
                            }
                        }
                        Console.WriteLine("Battle ended");
                        OutputResults(resultsWriter, results, battle);
                        if (USE_DB)
                        {
                            OutputResultsToDb(dbConnection, results, battle);
                        }
                        sumResults.Add(results);
                    }
                }
                OutputFinalBenchmarkResults(sumResults);
                Console.WriteLine($"Total benchmark time: {totalTime.Elapsed}\n");
            }
        }

        private void OutputFinalBenchmarkResults(Results results)
        {
            var sumResults = results.WinCounts;
            var sumSymetricResults = results.SymWinCounts;
            var sumHullResults = results.HullRemaining;
            Console.WriteLine("Benchmark ended");
            Console.WriteLine($"{sumResults[0]} wins, {sumSymetricResults[0]} symWins, {sumHullResults[0]} hull {_players[0]}");
            Console.WriteLine($"{sumResults[1]} wins, {sumSymetricResults[1]} symWins, {sumHullResults[1]} hull {_players[1]}");
            if (sumResults[2] != 0)
                Console.WriteLine($"Draws: {sumResults[2]}");
            if (sumSymetricResults[2] != 0)
                Console.WriteLine($"SymDraws: {sumSymetricResults[2]}");
        }

        private static void SwapUnitsAndActive(GameEnvironment envi)
        {
            var tmp = envi.GameState.Units[0];
            envi.GameState.Units[0] = envi.GameState.Units[1];
            envi.GameState.Units[1] = tmp;

            envi.GameState.ActivePlayer = GameState.Opo(envi.GameState.ActivePlayer);
        }

        private void OutputResultsToDb(NpgsqlConnection dbConnection, Results results, BattleSettings battle)
        {
            Thread.CurrentThread.CurrentCulture = CultureInfo.InvariantCulture;
            using (var cmd = new NpgsqlCommand())
            {
                cmd.Connection = dbConnection;
                cmd.CommandText = "INSERT INTO battles("
                                  + "ai_1_bs_unit_count, ai_2_bs_unit_cout, ai_1_d_unit_count, ai_2_d_unit_count, repeats, ai_1_win, ai_2_win, ai_1_sym_win, ai_2_sym_win, ai_1_hull, ai_2_hull, min_rounds, median_rounds, max_rounds, avg_rounds, ai_1_min_round_time, ai_1_max_round_time, ai_1_median_round_time, ai_1_avg_round_time, ai_2_min_round_time, ai_2_max_round_time, ai_2_median_round_time, ai_2_avg_round_time, ai_1, ai_2)"
                                  + "VALUES(" +
                                  $"{BattleUtil.GetBattleshipCount(battle.UnitCounts[0])}, " +
                                  $"{BattleUtil.GetBattleshipCount(battle.UnitCounts[1])}, " +
                                  $"{BattleUtil.GetDestroyerCount(battle.UnitCounts[0])}, " +
                                  $"{BattleUtil.GetDestroyerCount(battle.UnitCounts[1])}, " +
                                  $"{battle.Repeats}, " +
                                  $"{results.WinCounts[0]}, " +
                                  $"{results.WinCounts[1]}, " +
                                  $"{results.SymWinCounts[0]}, " +
                                  $"{results.SymWinCounts[1]}, " +
                                  $"{results.HullRemaining[0]}, " +
                                  $"{results.HullRemaining[1]}, " +
                                  $"{results.RoundCounts.Min()}, " +
                                  $"{results.RoundCounts.Median()}, " +
                                  $"{results.RoundCounts.Max()}, " +
                                  $"{results.RoundCounts.Average()}, " +
                                  $"{results.RoundTimes[0].Min()}, " +
                                  $"{results.RoundTimes[0].Max()}, " +
                                  $"{results.RoundTimes[0].Median()}, " +
                                  $"{results.RoundTimes[0].Average()}, " +
                                  $"{results.RoundTimes[1].Min()}, " +
                                  $"{results.RoundTimes[1].Max()}, " +
                                  $"{results.RoundTimes[1].Median()}, " +
                                  $"{results.RoundTimes[1].Average()}, " +
                                  $"'{_playerNames[0]}', " +
                                  $"'{_playerNames[1]}');";
                //Console.WriteLine(cmd.CommandText);
                cmd.ExecuteNonQuery();
            }
        }

        private void OutputResults(StreamWriter sw, Results results, BattleSettings battle)
        {
            Thread.CurrentThread.CurrentCulture = CultureInfo.InvariantCulture;
            sw.WriteLine(string.Join(";",
                battle.Name,
                battle.Repeats,
                results.WinCounts[0],
                results.WinCounts[1],
                results.SymWinCounts[0],
                results.SymWinCounts[1],
                results.HullRemaining[0],
                results.HullRemaining[1],
                results.RoundCounts.Min(),
                results.RoundCounts.Max(),
                results.RoundCounts.Median(),
                results.RoundCounts.Average(),
                results.RoundTimes[0].Min(),
                results.RoundTimes[0].Max(),
                results.RoundTimes[0].Median(),
                results.RoundTimes[0].Average(),
                results.RoundTimes[1].Min(),
                results.RoundTimes[1].Max(),
                results.RoundTimes[1].Median(),
                results.RoundTimes[1].Average()
            ));
        }
    }
}
